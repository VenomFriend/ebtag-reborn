var message = argument[0];
var client = instance_find(objClient, 0);
if(client) {
    with(client){
        for(var i = 4; i > 0; i--) {
            chatText[i] = chatText[i - 1];
        }
        
        chatText[0] = message;
    }
}
