///ServerSendChatExclusive(text, playerNotToSend);

var text = argument[0];
var playerToSend = argument[1];

// Tell everyone to create the wall
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_CHAT);
buffer_write(global.bufferServerWrite, buffer_string, text);

network_send_packet(playerToSend, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
