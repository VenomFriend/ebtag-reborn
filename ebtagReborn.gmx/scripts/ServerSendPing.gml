///ServerSendPing();
var socketSize = ds_list_size(global.socketList);
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_s8, MSG_PING);


for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        var isAlive = network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        if(isAlive < 0) {
            scrRemovePlayer(thisSocket);
        }
}
