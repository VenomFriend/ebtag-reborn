var character = argument[0];
var ip = argument[1];
var name = argument[2];
if (file_exists("char.sav")) {
    ini_open("char.sav");
    if (ini_section_exists("player")) {
        character.bodySelected = ini_read_real("player", "Body", 1);
        character.hairSelected = ini_read_real("player", "Hair", 0);
        character.hairRed = ini_read_real("player", "HairRed", 0);
        character.hairGreen = ini_read_real("player", "HairGreen", 0);
        character.hairBlue = ini_read_real("player", "HairBlue", 0);
        character.shirtSelected = ini_read_real("player", "Shirt", 0);
        character.shirtRed = ini_read_real("player", "ShirtRed", 0);
        character.shirtGreen = ini_read_real("player", "ShirtGreen", 0);
        character.shirtBlue = ini_read_real("player", "ShirtBlue", 0);
        character.pantsSelected = ini_read_real("player", "Pants", 0);
        character.pantsRed = ini_read_real("player", "PantsRed", 0);
        character.pantsGreen = ini_read_real("player", "PantsGreen", 0);
        character.pantsBlue = ini_read_real("player", "PantsBlue", 0);
        character.shoesSelected = ini_read_real("player", "Shoes", 0);
        character.shoesRed = ini_read_real("player", "ShoesRed", 0);
        character.shoesGreen = ini_read_real("player", "ShoesGreen", 0);
        character.shoesBlue = ini_read_real("player", "ShoesBlue", 0);
        character.accSelected = ini_read_real("player", "Acc", 0);
        character.accRed = ini_read_real("player", "AccRed", 0);
        character.accGreen = ini_read_real("player", "AccGreen", 0);
        character.accBlue = ini_read_real("player", "AccBlue", 0);
        character.sex = ini_read_real("player", "Sex", 0);
        ip.text = ini_read_string("info", "IP", "");
        name.text = ini_read_string("info", "Name", "");
    }
    ini_close();
}
