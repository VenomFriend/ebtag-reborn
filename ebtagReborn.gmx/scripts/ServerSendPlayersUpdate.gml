var socketSize = ds_list_size(global.socketList);

for (var i =0; i<socketSize; i++) {
    var thisPlayerID = ds_list_find_value(global.socketList, i);
    var inst = ds_map_find_value(global.Clients, thisPlayerID);
    
    
    if !is_undefined(inst) {
        //Only send the info of the players who actually changed something, to avoid wasting resources!
        if (inst.x != inst.xprevious) or (inst.y != inst.yprevious) or (inst.image_index != inst.currentImageIndex)
        or inst.isIT != inst.isITPrevious or inst.isConfused != inst.isConfusedPrevious 
        or inst.canMove != inst.canMovePrevious{
            inst.currentImageIndex = inst.image_index; 
            inst.isITPrevious = inst.isIT;
            inst.isConfusedPrevious = inst.isConfused;
            inst.canMovePrevious = inst.canMove;
            isMovingPrevious = inst.isMoving;
            buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
            buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_PLAYERS);
            buffer_write(global.bufferServerWrite, buffer_u32, thisPlayerID);
            buffer_write(global.bufferServerWrite, buffer_u16, inst.x);
            buffer_write(global.bufferServerWrite, buffer_u16, inst.y);
            buffer_write(global.bufferServerWrite, buffer_u16, inst.image_index);
            buffer_write(global.bufferServerWrite, buffer_bool, inst.isIT);
            buffer_write(global.bufferServerWrite, buffer_bool, inst.isConfused);
            buffer_write(global.bufferServerWrite, buffer_bool, inst.canMove);
            
            for(var ii = 0; ii<socketSize; ii++) {
                var thisSocket = ds_list_find_value(global.socketList, ii);
                if (thisPlayerID != thisSocket) {
                    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
                }
            }
        }
    }
}
