enum soundEffects {
    drain = 0,
    teleport = 1,
    solarFlare = 2,
    barrier = 3,
    freeza = 4,
    wall = 5,
    ghostbusters = 6,
    confusion = 7,
    badDice = 8,
    cloneWars = 9
}

enum playerMovement {
    downStart = 0,
    downEnd = 2,
    rightStart = 3,
    rightEnd = 5,
    upStart =   6,
    upEnd = 8,
    leftStart = 9,
    leftEnd = 11
}

enum ITPowers {
    theWall = 0,
    ghostbusters = 1,
    vampirism = 2,
    confusion = 3,
    solarFlare = 4
}

enum playerPowers {
    theWall = 0,
    goodDice = 1,
    barrier = 2,
    ghostbusters = 3,
    freeza = 4,
    cloneWars = 5,
    compass = 6,
    badDice = 7 
}

enum playerDirection {
    up = 0,
    down = 1,
    left = 2,
    right = 3,
    upLeft = 4,
    upRight = 5,
    downLeft = 6,
    downRight = 7
}

enum gameMap {
    forest = 0,
    ice = 1,
    dungeon = 2,
    desert = 3
}
