///ClientReceivedPacket(buffer)
var bufferClientReceived = argument[0]
var msgid = buffer_read(bufferClientReceived, buffer_u8);

switch(msgid) {
    //Set my ID to the one the server sent me
    //And send all my information to the server!
    case MSG_CREATE_ID:
        //In the future, I should delete this global variable and just
        //create a local variable myID on the player instance, and use it 
        //like myPlayerInstance.myID
        //I mean, it's kinda idiot to use a global variable without needing it,
        //but when I first created this code I was basing it on my old game, which 
        //had a lot of global stuff
        //but global stuff can become horrible messy later, at least that's
        //what usually happens with me
        global.myPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var mapToGo = buffer_read(bufferClientReceived, buffer_u8);
        var mapX = buffer_read(bufferClientReceived, buffer_u32);
        var mapY = buffer_read(bufferClientReceived, buffer_u32);
        //myPlayerInstance is a variable on the
        //objClient that has stored the ID of the 
        //objPlayer, and since this script is executed
        //by the objClient, I can use it here
        myPlayerInstance.x = mapX;
        myPlayerInstance.y = mapY;
        scrGoToMap(mapToGo);
        ClientSendPlayer(myPlayerInstance);
        break;
        
        
    //You became IT
    case MSG_IS_IT:
        var inst = instance_find(objPlayer, 0);
        inst.isIT = true;
        scrChatClient("You are now IT!");
        break;
        
    //Create all the players!
    case MSG_CREATE_PLAYER:
        var playerCount = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < playerCount; i++) {
            var playerID = buffer_read(bufferClientReceived, buffer_u32);
            var playerX = buffer_read(bufferClientReceived, buffer_u16);
            var playerY = buffer_read(bufferClientReceived, buffer_u16);
            var playerSprite = buffer_read(bufferClientReceived, buffer_u16);
            var playerImage = buffer_read(bufferClientReceived, buffer_u16);
            var playerHairReceived = buffer_read(bufferClientReceived, buffer_s8);
            var playerShirtReceived = buffer_read(bufferClientReceived, buffer_s8);
            var playerPantsReceived = buffer_read(bufferClientReceived, buffer_s8);
            var playerShoesReceived = buffer_read(bufferClientReceived, buffer_s8);
            var playerAccReceived = buffer_read(bufferClientReceived, buffer_s8);
            var playerHairColorReceived = buffer_read(bufferClientReceived, buffer_s32);
            var playerShirtColorReceived = buffer_read(bufferClientReceived, buffer_s32);
            var playerPantsColorReceived = buffer_read(bufferClientReceived, buffer_s32);
            var playerShoesColorReceived = buffer_read(bufferClientReceived, buffer_s32);
            var playerAccColorReceived = buffer_read(bufferClientReceived, buffer_s32);
            var playerNameReceived = buffer_read(bufferClientReceived, buffer_string);
            var playerIsIT = buffer_read(bufferClientReceived, buffer_bool);
            var playerHasBarrier = buffer_read(bufferClientReceived, buffer_bool);
            var playerIsInvisible = buffer_read(bufferClientReceived, buffer_u8);
            var playerIsConfused = buffer_read(bufferClientReceived, buffer_bool);
            
            //If the player wasn't already created and it's not myself, create him!
            if ( (playerID == global.myPlayerID) ) {
                break;
            } else if (is_undefined(ds_map_find_value(global.players, playerID))) {
                var newPlayer = instance_create(playerX, playerY, objDummieClient);
                with (newPlayer) {
                    isIT = playerIsIT;
                    hasBarrier = playerHasBarrier;
                    myID = playerID;
                    isInvisible = playerIsInvisible;
                    isConfused = playerIsConfused;
                    sprite_index = playerSprite;
                    image_index = playerImage;
                    playerHair = playerHairReceived;
                    playerShirt = playerShirtReceived;
                    playerPants = playerPantsReceived;
                    playerShoes = playerShoesReceived;
                    playerAcc = playerAccReceived;
                    playerHairColor = playerHairColorReceived;
                    playerShirtColor = playerShirtColorReceived;
                    playerPantsColor = playerPantsColorReceived;
                    playerShoesColor = playerShoesColorReceived;
                    playerAccColor = playerAccColorReceived;
                    playerName = playerNameReceived;
                    
                }
                ds_map_add(global.players, playerID, newPlayer);
            } else {
                var thisPlayer = ds_map_find_value(global.players, playerID);
                with (thisPlayer) {
                    isIT = playerIsIT;
                    hasBarrier = playerHasBarrier;
                    isInvisible = playerIsInvisible;
                    isConfused = playerIsConfused;
                    sprite_index = playerSprite;
                    image_index = playerImage;
                    playerHair = playerHairReceived;
                    playerShirt = playerShirtReceived;
                    playerPants = playerPantsReceived;
                    playerShoes = playerShoesReceived;
                    playerAcc = playerAccReceived;
                    playerHairColor = playerHairColorReceived;
                    playerShirtColor = playerShirtColorReceived;
                    playerPantsColor = playerPantsColorReceived;
                    playerShoesColor = playerShoesColorReceived;
                    playerAccColor = playerAccColorReceived;
                    playerName = playerNameReceived;
                }
            }        
        }
        break;
        
    //A player disconnected, so delete him!
    case MSG_DELETE_PLAYER:
        thisPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        if (!is_undefined(ds_map_find_value(global.players, thisPlayerID))){
            var inst = ds_map_find_value(global.players, thisPlayerID);
            ds_map_delete(global.players, thisPlayerID);
            with (inst) {
                instance_destroy();
            }
        }
        
        // If you are not IT and you have the red dice power, remove it if 
        // there are less than 3 players
        var inst = instance_find(objPlayer, 0);
        var playerCount = instance_number(objDummieClient);
        if (inst.isIT == false and playerCount < 2 and inst.powerSelected == playerPowers.badDice) {
            inst.powerSelected = -1;
        }
        
        break;
        
        
        
    //Someone moved/changed sprite/received damage, update it!    
    case MSG_UPDATE_PLAYERS:
        var thisID = buffer_read(bufferClientReceived, buffer_u32);
        var thisX = buffer_read(bufferClientReceived, buffer_u16);
        var thisY = buffer_read(bufferClientReceived, buffer_u16);
        var thisImage = buffer_read(bufferClientReceived, buffer_u16);
        var playerIsIT = buffer_read(bufferClientReceived, buffer_bool);
        var playerIsConfused = buffer_read(bufferClientReceived, buffer_bool);
        var playerCanMove = buffer_read(bufferClientReceived, buffer_bool);
        if (is_undefined(ds_map_find_value(global.players, thisID)) and thisID != global.myPlayerID )
        {         
            var inst = instance_create(thisX, thisY, objDummieClient);
            inst.isIT = playerIsIT;
            ds_map_add(global.players, thisID, inst);
        }
        else {
            var inst = ds_map_find_value(global.players, thisID);
            inst.isIT = playerIsIT;
        }
        //This checking is kinda redundant, since it's impossible
        //to receive this packet with my ID, but whatever
        if (thisID != global.myPlayerID) {
            inst.x = thisX;
            inst.y = thisY;
            inst.image_index = thisImage;   
            inst.isIT = playerIsIT;
            inst.isConfused = playerIsConfused;
            inst.canMove = playerCanMove;
        }
        break;
        
    //Someone started/stoped moving, update it 
    case MSG_STATUS:
        var thisID = buffer_read(bufferClientReceived, buffer_u32);
        var thisIsMoving = buffer_read(bufferClientReceived, buffer_bool);
        if (is_undefined(ds_map_find_value(global.players, thisID)) and thisID != global.myPlayerID )
        {         
            var inst = instance_create(thisX, thisY, objDummieClient);
            ds_map_add(global.players, thisID, inst);
        }
        else {
            var inst = ds_map_find_value(global.players, thisID);
        }
        inst.isMoving = thisIsMoving;
        break;
    
    // OH GOD I CANT SEE OH GOD HELP ME 
    //(technically this thing here shouldn't be needed, since this message will never be sent to the IT who did this
    //but just in case I add another gamemode with more ITs, this is here)
    case MSG_SOLAR_FLARE:
        var viewX = buffer_read(bufferClientReceived, buffer_u32);
        var viewY = buffer_read(bufferClientReceived, buffer_u32);
        if (myPlayerInstance.isIT == false) {
            if (myPlayerInstance.x >= viewX and myPlayerInstance.x <= viewX + 640 
            and myPlayerInstance.y >= viewY and myPlayerInstance.y <= viewY + 480) {    
                with(myPlayerInstance) {
                    // If you have the barrier, you won't receive this
                    if (hasBarrier == false) {
                        solarFlare = true;
                        alarm[1] = 3 * room_speed;
                    }
                }
            }
        }
        break;
        
    //Create all the walls!
    case MSG_CREATE_WALL:
        var wallsToCreate = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < wallsToCreate; i++) {
            var wallID = buffer_read(bufferClientReceived, buffer_u32);
            var wallX = buffer_read(bufferClientReceived, buffer_u32);
            var wallY = buffer_read(bufferClientReceived, buffer_u32);
            var wallImage = buffer_read(bufferClientReceived, buffer_u32);
            
            //If the wall don't exist, create it
            var wallCount = instance_number(objWallClient);
            var wallExists = false;
            for(var i = 0; i < wallCount; i++){
                var thisWall = instance_find(objWallClient, i);
                if thisWall.myID == wallID {
                    wallExists = true;
                    break;
                }
            }
            if (wallExists == false) {
                var newWall = instance_create(wallX, wallY, objWallClient);
                with (newWall) {
                    myID = wallID;
                    image_index = wallImage;
                }
            }     
        }
        break; 
    case MSG_DELETE_WALL:
        var wallToDestroy = buffer_read(bufferClientReceived, buffer_u32);
        var wallCount = instance_number(objWallClient);
        for(var i = 0; i < wallCount; i++) {
            var thisWall = instance_find(objWallClient, i);
            if thisWall.myID == wallToDestroy {
                with (thisWall) {
                    instance_destroy();
                }
                break;
            }
        }
        break;  
    
    // Make the IT invisible/visible
    case MSG_GHOSTBUSTERS:
        var invisiblePlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var isInvisible = buffer_read(bufferClientReceived, buffer_u8);
        
        //the second checking should ALWAYS be false, since the server SHOULDN'T be sending
        //this message to the IT, but just in case...
        if (!is_undefined(ds_map_find_value(global.players, invisiblePlayerID)) and invisiblePlayerID != global.myPlayerID )
        {         
            var inst = ds_map_find_value(global.players, invisiblePlayerID);
            inst.isInvisible = isInvisible;
        }
        
        break;  
    // I really don't need to check if the guy is the IT, since this message shouldn't be sent to the IT guy
    // but just in case.... 
    case MSG_VAMPIRISM:
        if (myPlayerInstance.isIT == false) {
            with(myPlayerInstance) {
                if (hasBarrier == false) {
                    stamina -= 20;
                }
            }
        }
        break;
    // I really don't need to check if the guy is the IT, since this message shouldn't be sent to the IT guy
    // but just in case.... 
    case MSG_CONFUSION:
        var viewX = buffer_read(bufferClientReceived, buffer_u32);
        var viewY = buffer_read(bufferClientReceived, buffer_u32);
        if (myPlayerInstance.isIT == false) {
            if (myPlayerInstance.x >= viewX and myPlayerInstance.x <= viewX + 640 
            and myPlayerInstance.y >= viewY and myPlayerInstance.y <= viewY + 480) {  
            
                with(myPlayerInstance) {
                    // Only become confused if I don't have the barrier
                    if (hasBarrier == false) {
                        isConfused = true;
                        alarm[4] = 1; // Change player controls
                    }
                }
                ClientSendStatus(myPlayerInstance); // Just tell the server that I am confused, so he can tell everybody
            }
        }
        break;
    
    //Create all the powers!
    case MSG_CREATE_POWERS:
        var powersToCreate = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < powersToCreate; i++) {
            var powerID = buffer_read(bufferClientReceived, buffer_u32);
            var powerX = buffer_read(bufferClientReceived, buffer_u32);
            var powerY = buffer_read(bufferClientReceived, buffer_u32);
            var powerSelected = buffer_read(bufferClientReceived, buffer_u32);
            var powerIsVisible = buffer_read(bufferClientReceived, buffer_bool);
            
            //If the power don't exist, create it
            var powerCount = instance_number(objPowersClient);
            var powerExists = false;
            for(var i = 0; i < powerCount; i++){
                var thisPower = instance_find(objPowersClient, i);
                if thisPower.myID == powerID {
                    powerExists = true;
                    break;
                }
            }
            if (powerExists == false) {
                var newPower = instance_create(powerX, powerY, objPowersClient);
                newPower.myID = powerID;
                newPower.powerSelected = powerSelected;
                newPower.isVisible = powerIsVisible;
            }     
        }
        break;
    // Update the power, making it visible/invisible or just changing the power
    case MSG_UPDATE_POWER:
        var powerID = buffer_read(bufferClientReceived, buffer_u32);
        var powerSelected = buffer_read(bufferClientReceived, buffer_u32);
        var powerIsVisible = buffer_read(bufferClientReceived, buffer_bool);
        var powerCount = instance_number(objPowersClient);
        for (var i = 0; i < powerCount; i++){
            var thisPower = instance_find(objPowersClient, i);
            if (thisPower.myID == powerID){
                thisPower.powerSelected = powerSelected;
                thisPower.isVisible = powerIsVisible;
                break;
            }
        }
        break;
    // Used the Good Dice, now teleport me!
    case MSG_GOOD_DICE:
        var xToTeleport = buffer_read(bufferClientReceived, buffer_u32);
        var yToTeleport = buffer_read(bufferClientReceived, buffer_u32);
        myPlayerInstance.x = xToTeleport;
        myPlayerInstance.y = yToTeleport;
        break;
    // Show the barrier on the player
    case MSG_BARRIER:
        var thisPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var playerHasBarrier = buffer_read(bufferClientReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.players, thisPlayerID))){
            var inst = ds_map_find_value(global.players, thisPlayerID);
            inst.hasBarrier = playerHasBarrier;
        }     
        break;
    // Create a barrier on myself
    case MSG_BARRIER_REVERSE:
        // Just to make sure that I'm IT when this power arrives
        if(myPlayerInstance.isIT) {
            myPlayerInstance.hasBarrier = true;
            myPlayerInstance.alarm[6] = room_speed * 12;
        }
        break;
    // Create the freeza power!
    case MSG_CREATE_FREEZA:
        var freezaID = buffer_read(bufferClientReceived, buffer_u32);
        var freezaX = buffer_read(bufferClientReceived, buffer_u32);
        var freezaY = buffer_read(bufferClientReceived, buffer_u32);
        var freezaCount = instance_number(objFreezaClient);
        var freezaExists = false;
        for (var i = 0; i < freezaCount; i++) {
            var thisFreeza = instance_find(objFreezaClient, i);
            if thisFreeza.myID = freezaID {
                freezaExists = true;
                thisFreeza.x = freezaX;
                thisFreeza.y = freezaY;
            }
        }
        if (freezaExists == false) {
            var thisFreeza = instance_create(freezaX, freezaY, objFreezaClient);
            thisFreeza.myID = freezaID;
        }
        break;
    
    // Update the freeza power
    // Yeah, I know this shit is equal to the CREATE_FREEZA
    // but just to stay organized, I'll let this be
    case MSG_UPDATE_FREEZA:
        var freezaID = buffer_read(bufferClientReceived, buffer_u32);
        var freezaX = buffer_read(bufferClientReceived, buffer_u32);
        var freezaY = buffer_read(bufferClientReceived, buffer_u32);
        var freezaCount = instance_number(objFreezaClient);
        var freezaExists = false;
        for (var i = 0; i < freezaCount; i++) {
            var thisFreeza = instance_find(objFreezaClient, i);
            if thisFreeza.myID = freezaID {
                freezaExists = true;
                thisFreeza.x = freezaX;
                thisFreeza.y = freezaY;
            }
        }
        if (freezaExists == false) {
            var thisFreeza = instance_create(freezaX, freezaY, objFreezaClient);
            thisFreeza.myID = freezaID;
        }
        break;
        
    // FREEZAAAAAAAAA
    case MSG_DELETE_FREEZA:
        var freezaToDestroy = buffer_read(bufferClientReceived, buffer_u32);
        var freezaCount = instance_number(objFreezaClient);
        for(var i = 0; i < freezaCount; i++) {
            var thisFreeza = instance_find(objFreezaClient, i);
            if thisFreeza.myID == freezaToDestroy {
                with (thisFreeza) {
                    instance_destroy();
                }
                break;
            }
        }
        break; 
        
    //You froze
    case MSG_FREEZE_IT:
        // Maybe I should use the variable myPlayerInstance instead of looking for the objPlayer instance.... but who knows
        var inst = instance_find(objPlayer, 0);
        if (inst.isIT) {
            with(inst) {
                canMove = false;
                alarm[0] = 3 * room_speed;
            }
            ClientSendStatus(inst.id);
        }
        break;
    
    // Used the Bad Dice, now change my position
    case MSG_BAD_DICE:
        var xToTeleport = buffer_read(bufferClientReceived, buffer_u32);
        var yToTeleport = buffer_read(bufferClientReceived, buffer_u32);
        myPlayerInstance.x = xToTeleport;
        myPlayerInstance.y = yToTeleport;
        break;
        
    // Lose my power, because someone stole it from me
    case MSG_LOSE_POWER:
        myPlayerInstance.powerSelected = -1;
        break;
        
    case MSG_STEAL_POWER:
        var powerStole = buffer_read(bufferClientReceived, buffer_s8);
        myPlayerInstance.powerSelected = powerStole;
        break;
        
    case MSG_SOUND:
        var soundNumber = buffer_read(bufferClientReceived, buffer_u8);
        var soundX = buffer_read(bufferClientReceived, buffer_u32);
        var soundY = buffer_read(bufferClientReceived, buffer_u32);
        switch(soundNumber) {
            case soundEffects.drain:
                instance_create(soundX, soundY, objSoundDrain);
                break;
            case soundEffects.teleport:
                instance_create(soundX, soundY, objSoundTeleport);
                break;
            case soundEffects.solarFlare:
                instance_create(soundX, soundY, objSoundSolarFlare);
                break;
            case soundEffects.barrier:
                instance_create(soundX, soundY, objSoundBarrier);
                break;
            case soundEffects.freeza:
                instance_create(soundX, soundY, objSoundFreeza);
                break;
            case soundEffects.wall:
                instance_create(soundX, soundY, objSoundWall);
                break;
            case soundEffects.ghostbusters:
                instance_create(soundX, soundY, objSoundGhostbusters);
                break;
            case soundEffects.confusion:
                instance_create(soundX, soundY, objSoundConfusion);
                break;
            case soundEffects.badDice:
                instance_create(soundX, soundY, objSoundBadDice);
                break;
        }
        break;
        
    //Create all the blue walls!
    case MSG_CREATE_BLUE_WALLS:
        var blueWallsToCreate = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < blueWallsToCreate; i++) {
            var blueWallID = buffer_read(bufferClientReceived, buffer_u32);
            var blueWallX = buffer_read(bufferClientReceived, buffer_u32);
            var blueWallY = buffer_read(bufferClientReceived, buffer_u32);
            var blueWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
            var blueWallPosition = buffer_read(bufferClientReceived, buffer_u8);
            
            //If the blue wall don't exist, create it
            var blueWallCount = instance_number(objBlueWallClient);
            var blueWallExist = false;
            for(var i = 0; i < blueWallCount; i++){
                var thisBlueWall = instance_find(objBlueWallClient, i);
                if thisBlueWall.myID == blueWallID {
                    blueWallExist = true;
                    break;
                }
            }
            if (blueWallExist == false) {
                var newBlueWall = instance_create(blueWallX, blueWallY, objBlueWallClient);
                newBlueWall.myID = blueWallID;
                newBlueWall.isSolid = blueWallIsSolid;
                newBlueWall.position = blueWallPosition;
            }     
        }
        break;
        
    // Update the blue walls, making it solid/not solid
    case MSG_UPDATE_BLUE_WALL:
        var blueWallID = buffer_read(bufferClientReceived, buffer_u32);
        var blueWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
        var blueWallCount = instance_number(objBlueWallClient);
        for (var i = 0; i < blueWallCount; i++){
            var thisBlueWall = instance_find(objBlueWallClient, i);
            if (thisBlueWall.myID == blueWallID){
                thisBlueWall.isSolid = blueWallIsSolid;
                break;
            }
        }
        break;
        
    //Create all the yellow walls!
    case MSG_CREATE_YELLOW_WALLS:
        var yellowWallsToCreate = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < yellowWallsToCreate; i++) {
            var yellowWallID = buffer_read(bufferClientReceived, buffer_u32);
            var yellowWallX = buffer_read(bufferClientReceived, buffer_u32);
            var yellowWallY = buffer_read(bufferClientReceived, buffer_u32);
            var yellowWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
            var yellowWallPosition = buffer_read(bufferClientReceived, buffer_u8);
            
            //If the yellow wall don't exist, create it
            var yellowWallCount = instance_number(objYellowWallClient);
            var yellowWallExist = false;
            for(var i = 0; i < yellowWallCount; i++){
                var thisYellowWall = instance_find(objYellowWallClient, i);
                if thisYellowWall.myID == yellowWallID {
                    yellowWallExist = true;
                    break;
                }
            }
            if (yellowWallExist == false) {
                var newYellowWall = instance_create(yellowWallX, yellowWallY, objYellowWallClient);
                newYellowWall.myID = yellowWallID;
                newYellowWall.isSolid = yellowWallIsSolid;
                newYellowWall.position = yellowWallPosition;
            }     
        }
        break;
        
    // Update the yellow walls, making it solid/not solid
    case MSG_UPDATE_YELLOW_WALL:
        var yellowWallID = buffer_read(bufferClientReceived, buffer_u32);
        var yellowWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
        var yellowWallCount = instance_number(objYellowWallClient);
        for (var i = 0; i < yellowWallCount; i++){
            var thisYellowWall = instance_find(objYellowWallClient, i);
            if (thisYellowWall.myID == yellowWallID){
                thisYellowWall.isSolid = yellowWallIsSolid;
                break;
            }
        }
        break; 
       
    //Create all the friend walls AND the friend buttons!
    case MSG_CREATE_FRIEND_WALLS:
        var friendWallsToCreate = buffer_read(bufferClientReceived, buffer_u16);
        var friendButtonsToCreate = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < friendWallsToCreate; i++) {
            var friendWallID = buffer_read(bufferClientReceived, buffer_u32);
            var friendWallX = buffer_read(bufferClientReceived, buffer_u32);
            var friendWallY = buffer_read(bufferClientReceived, buffer_u32);
            var friendWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
            var friendWallPosition = buffer_read(bufferClientReceived, buffer_u8);
            
            //If the friend wall don't exist, create it
            var friendWallCount = instance_number(objFriendWallClient);
            var friendWallExist = false;
            for(var i = 0; i < friendWallCount; i++){
                var thisFriendWall = instance_find(objFriendWallClient, i);
                if thisFriendWall.myID == friendWallID {
                    friendWallExist = true;
                    break;
                }
            }
            if (friendWallExist == false) {
                var newFriendWall = instance_create(friendWallX, friendWallY, objFriendWallClient);
                newFriendWall.myID = friendWallID;
                newFriendWall.isSolid = friendWallIsSolid;
                newFriendWall.position = friendWallPosition;
            }     
        }
        for (var i = 0; i < friendButtonsToCreate; i++) {
            var friendButtonID = buffer_read(bufferClientReceived, buffer_u32);
            var friendButtonX = buffer_read(bufferClientReceived, buffer_u32);
            var friendButtonY = buffer_read(bufferClientReceived, buffer_u32);
            var friendButtonIsPressed = buffer_read(bufferClientReceived, buffer_bool);
            
            //If the friend button don't exist, create it
            var friendButtonCount = instance_number(objFriendButtonClient);
            var friendButtonExist = false;
            for(var i = 0; i < friendButtonCount; i++){
                var thisFriendButton = instance_find(objFriendButtonClient, i);
                if thisFriendButton.myID == friendButtonID {
                    friendButtonExist = true;
                    break;
                }
            }
            if (friendButtonExist == false) {
                var newFriendButton = instance_create(friendButtonX, friendButtonY, objFriendButtonClient);
                newFriendButton.myID = friendButtonID;
                newFriendButton.isPressed = friendButtonIsPressed;
            }     
        }
        break;
        
    // Update the friend walls, making it solid/not solid
    case MSG_UPDATE_FRIEND_WALL:
        var friendWallID = buffer_read(bufferClientReceived, buffer_u32);
        var friendWallIsSolid = buffer_read(bufferClientReceived, buffer_bool);
        var friendWallCount = instance_number(objFriendWallClient);
        for (var i = 0; i < friendWallCount; i++){
            var thisFriendWall = instance_find(objFriendWallClient, i);
            if (thisFriendWall.myID == friendWallID){
                thisFriendWall.isSolid = friendWallIsSolid;
                break;
            }
        }
        break;
        
    // Update the friend buttons, making them pressed / not pressed
    case MSG_UPDATE_FRIEND_BUTTON:
        var friendButtonID = buffer_read(bufferClientReceived, buffer_u32);
        var friendButtonIsPressed = buffer_read(bufferClientReceived, buffer_bool);
        var friendButtonCount = instance_number(objFriendButtonClient);
        for (var i = 0; i < friendButtonCount; i++){
            var thisFriendButton = instance_find(objFriendButtonClient, i);
            if (thisFriendButton.myID == friendButtonID){
                thisFriendButton.isPressed = friendButtonIsPressed;
                break;
            }
        }
        break;     
        
    // Create/update the clones
    case MSG_CLONE:
        show_debug_message("ENTERED MSG_CLONE");
        var cloneID = buffer_read(bufferClientReceived, buffer_u32);
        var cloneX = buffer_read(bufferClientReceived, buffer_u16);
        var cloneY = buffer_read(bufferClientReceived, buffer_u16);
        var cloneImage = buffer_read(bufferClientReceived, buffer_u16);
        var cloneFather = buffer_read(bufferClientReceived, buffer_u32);
        var cloneNumber = instance_number(objDummieCloneClient);
        var cloneFound = false;
        for (var i = 0; i < cloneNumber; i++){
            var thisClone = instance_find(objDummieCloneClient, i);
            if thisClone.myID == cloneID {
                show_debug_message("FOUND AN EXISTING CLONE!");
                cloneFound = true;
                thisClone.x = cloneX;
                thisClone.y = cloneY;
                thisClone.image_index = cloneImage;
                break;
            }
        }
        if (!cloneFound) {
            show_debug_message("DID NOT FIND AN EXISTING CLONE");
            var thisClone = instance_create(cloneX, cloneY, objDummieCloneClient);
            thisClone.myID = cloneID;
            var playerNumber = instance_number(objDummieClient);
            for (var i = 0; i < playerNumber; i++){
                var thisPlayer = instance_find(objDummieClient, i);
                if thisPlayer.myID == cloneFather {
                    show_debug_message("FOUNDND THE CLONE FATHER");
                    thisClone.father = thisPlayer;
                    break;
                }
            }
            with(thisClone) {
                sprite_index = thisPlayer.sprite_index;
                cloneHair = thisPlayer.playerHair;
                cloneShirt = thisPlayer.playerShirt;
                clonePants = thisPlayer.playerPants;
                cloneShoes = thisPlayer.playerShoes;
                cloneAcc = thisPlayer.playerAcc;
                cloneHairColor = thisPlayer.playerHairColor;
                cloneShirtColor = thisPlayer.playerShirtColor;
                clonePantsColor = thisPlayer.playerPantsColor;
                cloneShoesColor = thisPlayer.playerShoesColor;
                cloneAccColor = thisPlayer.playerAccColor;
                image_index = cloneImage;
            }
            
        }
        break;
        
    // Delete the clone
    case MSG_DELETE_CLONE:
        show_debug_message("ENTERED DELETE CLONE");
        var cloneID = buffer_read(bufferClientReceived, buffer_u32);
        var cloneNumber = instance_number(objDummieCloneClient);
        var cloneFound = false;
        for (var i = 0; i < cloneNumber; i++){
            var thisClone = instance_find(objDummieCloneClient, i);
            if thisClone != noone and thisClone.myID == cloneID {
                show_debug_message("IS GOING TO DELETE A CLONE");
                with(thisClone) {
                    instance_destroy();
                }
            }
        }
        break; 
        
    // Change the map!
    case MSG_CHANGE_MAP:
        var mapToGo = buffer_read(bufferClientReceived, buffer_u8);
        var xToGo = buffer_read(bufferClientReceived, buffer_u32);
        var yToGo = buffer_read(bufferClientReceived, buffer_u32);
        scrClientDestroyAllObjects();
        myPlayerInstance.x = xToGo;
        myPlayerInstance.y = yToGo;
        scrGoToMap(mapToGo);
        ClientSendPosition(myPlayerInstance); // Update my position on the server
        break;    
        
    // Create the teleports on the map
    case MSG_CREATE_TELEPORT:
        var teleportsCount = buffer_read(bufferClientReceived, buffer_u16);
        for(var i = 0; i < teleportsCount; i++) {
            var teleportID = buffer_read(bufferClientReceived, buffer_u32);
            var teleportX = buffer_read(bufferClientReceived, buffer_u32);
            var teleportY = buffer_read(bufferClientReceived, buffer_u32);
            var teleportColor = buffer_read(bufferClientReceived, buffer_u8);
            var teleportGoToX = buffer_read(bufferClientReceived, buffer_u32);
            var teleportGoToY = buffer_read(bufferClientReceived, buffer_u32);
            var teleportGoToX2 = buffer_read(bufferClientReceived, buffer_u32);
            var teleportGoToY2 = buffer_read(bufferClientReceived, buffer_u32);
            var teleportGoToX3 = buffer_read(bufferClientReceived, buffer_u32);
            var teleportGoToY3 = buffer_read(bufferClientReceived, buffer_u32);
            var teleportRotate = buffer_read(bufferClientReceived, buffer_u8);
            var teleport = instance_create(teleportX, teleportY, objTeleportClient);
            var teleportCircle = instance_create(teleportGoToX, teleportGoToY, objTeleportCircleClient);
            teleport.color = teleportColor;
            teleport.rotate = teleportRotate;
            if(teleportColor == 1) {
                teleport.sprite_index = sprTeleportPurple;
            } else if(teleportColor == 2) {
                teleport.sprite_index = sprTeleportYellow;
            }
            if(teleport.rotate == 1) {
                teleport.image_angle = 90;
            } else if(teleport.rotate == 2) {
                teleport.image_angle = 270;
            }
            teleport.goToX = teleportGoToX;
            teleport.goToY = teleportGoToY;
            teleport.goToX2 = teleportGoToX2;
            teleport.goToY2 = teleportGoToY2;
            teleport.goToX3 = teleportGoToX3;
            teleport.goToY3 = teleportGoToY3;
            teleportCircle.image_index = teleportColor;
        }    
        break;              
    case MSG_CREATE_CHAT:
        var text = buffer_read(bufferClientReceived, buffer_string);
        scrChatClient(text);
        break;
}
