///scrDrawBars()

draw_sprite(sprBar, 0, view_xview[0] + 5, view_yview[0] + 5);
draw_sprite(sprBar, 0, view_xview[0] + 5, view_yview[0] + 26);

var numberOfBarsStamina = round(myPlayerInstance.stamina/5);
var numberOfBarsSpecial = round(myPlayerInstance.special/5);


for (var i = 0; i < numberOfBarsStamina; i++) {
    draw_sprite(sprStamina, 0, view_xview[0] + 19 + (i * 8), view_yview[0] + 5);
}
if (numberOfBarsStamina < 20) {
    draw_sprite(sprStamina, 1, view_xview[0] + 19 + (numberOfBarsStamina * 8), view_yview[0] + 5);
}
for (var i = 0; i < numberOfBarsSpecial; i++) {
    draw_sprite(sprSpecial, 0, view_xview[0] + 19 + (i * 8), view_yview[0] + 26);
}  
if (numberOfBarsSpecial < 20) {
    draw_sprite(sprSpecial, 1, view_xview[0] + 19 + (numberOfBarsSpecial * 8), view_yview[0] + 26);
}  
