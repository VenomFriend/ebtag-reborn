///scrRemovePlayer(socket);

var socket = argument[0];

//Find the instance of the player
var inst = ds_map_find_value(global.Clients, socket);
//Delete it from the map
ds_map_delete(global.Clients, socket);
//Tell everybody to delete this player
ServerSendDestroy(socket);
var text = inst.playerName + " disconnected.";
ServerSendChat(text, socket);

// If the disconnected player was IT, another player becomes IT
show_debug_message("SOMEONE DISCONNECTED");
show_debug_message("He was it = " + string(inst.isIT));


if (inst.isIT) {
    var doomedPlayer = instance_furthest(inst.x, inst.y, objDummieServer);
    if doomedPlayer != noone and doomedPlayer != inst {
        show_debug_message("ENTERED THE ISIT IF");
        doomedPlayer.isIT = true;
        ServerSendPlayersUpdate();
        ServerSendIsIT(doomedPlayer.myID);
    }
}

// If the disconnected player had clones, destroy the clones and tell everybody to do the same

var cloneNumbers = instance_number(objDummieCloneServer);
for (var i = cloneNumbers - 1; i >= 0; i--) { // If I do a normal i= 0; i < cloneNumbers; i++, it's not gonna work, I need to delete from the last to the first... sigh
    var thisClone = instance_find(objDummieCloneServer, i);
    if (thisClone != noone and thisClone.father == inst) {
        ServerSendDeleteClone(thisClone.myID, -1);
        with(thisClone) {
            instance_destroy();
        }
    }
}

// And delete the instance of the player from the server
with (inst) {
    instance_destroy()
}



var findSocket = ds_list_find_index(global.socketList, socket);
if (findSocket >= 0) {
    ds_list_delete(global.socketList, findSocket);
}

