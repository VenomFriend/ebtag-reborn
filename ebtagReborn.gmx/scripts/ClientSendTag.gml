///ClientSendTag(newTagID)

var newTagID = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_TAG);
buffer_write(global.bufferClientWrite, buffer_u32, newTagID);

network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
