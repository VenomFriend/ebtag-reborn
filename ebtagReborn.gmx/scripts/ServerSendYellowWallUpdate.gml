///ServerSendYellowWallUpdate(wall)
var thisYellowWall = argument[0];
var socketSize = ds_list_size(global.socketList);

// Tell everyone to update their yellow walls
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_YELLOW_WALL);
buffer_write(global.bufferServerWrite, buffer_u32, thisYellowWall.id);
buffer_write(global.bufferServerWrite, buffer_bool, thisYellowWall.isSolid);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
