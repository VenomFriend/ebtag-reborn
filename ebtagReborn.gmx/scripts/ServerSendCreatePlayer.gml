var socket = argument[0];
var mapToGo = argument[1];
var mapX = argument[2];
var mapY = argument[3];

// Tell the player who just connected his ID, the game map and his position
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_ID);
buffer_write(global.bufferServerWrite, buffer_u32, socket);
buffer_write(global.bufferServerWrite, buffer_u8, mapToGo);
buffer_write(global.bufferServerWrite, buffer_u32, mapX);
buffer_write(global.bufferServerWrite, buffer_u32, mapY);
network_send_packet(socket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

