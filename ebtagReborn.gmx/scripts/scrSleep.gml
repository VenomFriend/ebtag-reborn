if (!canMove) {
    sleepFrame += 0.2
    if sleepFrame >= 5 {
        sleepFrame = 0;
    }
} else if (canMove and sleepFrame != 0) {
    sleepFrame = 0;
}
