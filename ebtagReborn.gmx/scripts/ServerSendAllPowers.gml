///ServerSendAllPowers(socket)

var playerSocket = argument[0];

var powersCount = instance_number(objPowersServer);

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_POWERS);
buffer_write(global.bufferServerWrite, buffer_u16, powersCount);

for(var i = 0; i < powersCount; i++) {
    var powerItem = instance_find(objPowersServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, powerItem.id);
    buffer_write(global.bufferServerWrite, buffer_u32, powerItem.x);
    buffer_write(global.bufferServerWrite, buffer_u32, powerItem.y);
    buffer_write(global.bufferServerWrite, buffer_u32, powerItem.powerSelected);
    buffer_write(global.bufferServerWrite, buffer_bool, powerItem.isVisible);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

