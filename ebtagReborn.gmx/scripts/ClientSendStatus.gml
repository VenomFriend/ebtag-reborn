///ClientSendStatus(inst)
// All the status of the players are sent to the server here(except movement)
// I did this just cause I wanted the players to tell that they are confused
// but maybe I should do it for other stuff
// probably would need to redo some of the code, and I'm too lazy at the moment


var inst = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_STATUS);
buffer_write(global.bufferClientWrite, buffer_bool, inst.isConfused);
buffer_write(global.bufferClientWrite, buffer_bool, inst.canMove);
buffer_write(global.bufferClientWrite, buffer_bool, inst.isMoving);
buffer_write(global.bufferClientWrite, buffer_s8, inst.powerSelected);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
