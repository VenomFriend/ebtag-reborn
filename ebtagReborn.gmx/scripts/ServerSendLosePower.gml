/// ServerSendLosePower(playerSocket)

var playerSocket = argument[0];

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_LOSE_POWER);

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
