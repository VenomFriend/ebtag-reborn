///ClientSendGhostbusters(visibility)

var isInvisible = argument[0];

// Tell all the other players that the IT is invisible(but not invincible! oh wait, wrong game)

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_GHOSTBUSTERS);
buffer_write(global.bufferClientWrite, buffer_u8, isInvisible);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
