///ServerSendPowerUpdate(power)
var thisPower = argument[0];
var socketSize = ds_list_size(global.socketList);

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_POWER);
buffer_write(global.bufferServerWrite, buffer_u32, thisPower.id);
buffer_write(global.bufferServerWrite, buffer_u32, thisPower.powerSelected);
buffer_write(global.bufferServerWrite, buffer_bool, thisPower.isVisible);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
