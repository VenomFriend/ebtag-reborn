///ClientSendPlayer(instance)
// When you create your player, you must provide the server with all the information

var myPlayer = argument[0];


buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_CREATE_ID);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.x);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.y);
buffer_write(global.bufferClientWrite, buffer_u16, myPlayer.image_index);
buffer_write(global.bufferClientWrite, buffer_u32, myPlayer.sprite_index);
buffer_write(global.bufferClientWrite, buffer_s8, myPlayer.playerHair);
buffer_write(global.bufferClientWrite, buffer_s8, myPlayer.playerShirt);
buffer_write(global.bufferClientWrite, buffer_s8, myPlayer.playerPants);
buffer_write(global.bufferClientWrite, buffer_s8, myPlayer.playerShoes);
buffer_write(global.bufferClientWrite, buffer_s8, myPlayer.playerAcc);
buffer_write(global.bufferClientWrite, buffer_s32, myPlayer.playerHairColor);
buffer_write(global.bufferClientWrite, buffer_s32, myPlayer.playerShirtColor);
buffer_write(global.bufferClientWrite, buffer_s32, myPlayer.playerPantsColor);
buffer_write(global.bufferClientWrite, buffer_s32, myPlayer.playerShoesColor);
buffer_write(global.bufferClientWrite, buffer_s32, myPlayer.playerAccColor);
buffer_write(global.bufferClientWrite, buffer_string, myPlayer.playerName);

network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
