var socketSize = ds_list_size(global.socketList);

for (var i =0; i<socketSize; i++) {
    var thisPlayerID = ds_list_find_value(global.socketList, i);
    var inst = ds_map_find_value(global.Clients, thisPlayerID);
    
    
    if !is_undefined(inst) {
        //This is to make 10000% sure that, when the player is invisible when he stopped moving, he is invisible to everybody
        if inst.isMoving != inst.isMovingPrevious{
            inst.isMovingPrevious = inst.isMoving;
            buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
            buffer_write(global.bufferServerWrite, buffer_u8, MSG_STATUS);
            buffer_write(global.bufferServerWrite, buffer_u32, thisPlayerID);
            buffer_write(global.bufferServerWrite, buffer_bool, inst.isMoving);
            for(var ii = 0; ii<socketSize; ii++) {
                var thisSocket = ds_list_find_value(global.socketList, ii);
                if (thisPlayerID != thisSocket) {
                    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
                }
            }
        }
    }
}
