///ServerReceivedPacket(buffer, socket)
var bufferServerReceived = argument[0];
var socket = argument[1];
var msgid = buffer_read(bufferServerReceived, buffer_u8);

switch (msgid) {
    //The player who connected sent me all his information, so I must update it here!
    case MSG_CREATE_ID:
        var thisX = buffer_read(bufferServerReceived, buffer_u16);
        var thisY = buffer_read(bufferServerReceived, buffer_u16);
        var thisImage = buffer_read(bufferServerReceived, buffer_u16);
        var thisSprite = buffer_read(bufferServerReceived, buffer_u32);
        var thisHair = buffer_read(bufferServerReceived, buffer_s8);
        var thisShirt = buffer_read(bufferServerReceived, buffer_s8);
        var thisPants = buffer_read(bufferServerReceived, buffer_s8);
        var thisShoes = buffer_read(bufferServerReceived, buffer_s8);
        var thisAcc = buffer_read(bufferServerReceived, buffer_s8);
        var thisHairColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisShirtColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisPantsColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisShoesColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisAccColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisName = buffer_read(bufferServerReceived, buffer_string);
        
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            inst.x = thisX;
            inst.y = thisY;
            inst.image_index = thisImage;
            inst.sprite_index = thisSprite;
            inst.playerHair = thisHair;
            inst.playerShirt = thisShirt;
            inst.playerPants = thisPants;
            inst.playerShoes = thisShoes;
            inst.playerAcc = thisAcc;
            inst.playerName = thisName;
            inst.playerHairColor = thisHairColor;
            inst.playerShirtColor = thisShirtColor;
            inst.playerPantsColor = thisPantsColor;
            inst.playerShoesColor = thisShoesColor;
            inst.playerAccColor = thisAccColor;
            ServerSendNewPlayer(inst, socket);
            text = string(inst.playerName) + " connected!";
            ServerSendChat(text, socket);
        }
        
        break;

    // Someone moved, update every other player!
    case MSG_MOVEMENT:
        var thisX = buffer_read(bufferServerReceived, buffer_u16);
        var thisY = buffer_read(bufferServerReceived, buffer_u16);
        var thisImage = buffer_read(bufferServerReceived, buffer_u16);
        var isMoving = buffer_read(bufferServerReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            inst.x = thisX;
            inst.y = thisY;
            inst.image_index = thisImage;
            inst.isMoving = isMoving;
        }
           
        break;
        
    case MSG_TAG:
        var newTagID = buffer_read(bufferServerReceived, buffer_u32);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            inst.isIT = false;
        }
        if (!is_undefined(ds_map_find_value(global.Clients, newTagID))){
            var inst = ds_map_find_value(global.Clients, newTagID);
            inst.isIT = true;
            ServerSendIsIT(newTagID);
            var textToSend = string(inst.playerName) + " is now IT!";
            ServerSendChat(textToSend, newTagID); // Send chat message to everybody except the new IT
        }
        break;
        
    case MSG_SOLAR_FLARE:
        var viewX = buffer_read(bufferServerReceived, buffer_u32);
        var viewY = buffer_read(bufferServerReceived, buffer_u32);
        ServerSendSolarFlare(viewX, viewY, socket);
        break;
    // A wall was created, tell everybody about it
    case MSG_THE_WALL:
        var playerFacing = buffer_read(bufferServerReceived, buffer_u8);
        var wallX = buffer_read(bufferServerReceived, buffer_u32);
        var wallY = buffer_read(bufferServerReceived, buffer_u32);
        ServerCreateWall(playerFacing, wallX, wallY);
        break;
    // IT is invisible/visible, tell all the other players
    case MSG_GHOSTBUSTERS:
        var isInvisible = buffer_read(bufferServerReceived, buffer_u8);
        var inst = ds_map_find_value(global.Clients, socket);
        inst.isInvisible = isInvisible;
        ServerSendGhostbusters(isInvisible, socket);
        break;
    case MSG_VAMPIRISM:
        ServerSendVampirism(socket);
        break;
    case MSG_CONFUSION:
        var viewX = buffer_read(bufferServerReceived, buffer_u32);
        var viewY = buffer_read(bufferServerReceived, buffer_u32);
        ServerSendConfusion(viewX, viewY, socket);
        break;
    case MSG_STATUS:
        var isConfused = buffer_read(bufferServerReceived, buffer_bool);
        var canMove = buffer_read(bufferServerReceived, buffer_bool);
        var isMoving = buffer_read(bufferServerReceived, buffer_bool);
        var powerSelected = buffer_read(bufferServerReceived, buffer_s8);
        var inst = ds_map_find_value(global.Clients, socket);
        if (!is_undefined(inst)) {
            inst.isConfused = isConfused;
            inst.canMove = canMove;
            inst.isMoving = isMoving;
            inst.powerSelected = powerSelected;
        }
        break;
    case MSG_GOT_POWER:
        var powerGot = buffer_read(bufferServerReceived, buffer_u32);
        powerGot.isVisible = false;
        powerGot.alarm[0] = 5*room_speed;
        ServerSendPowerUpdate(powerGot);
        break;
        
    // Probably gonna create a script for this good dice stuff
    case MSG_GOOD_DICE:
        if(room == rm_game) {
            var pointAx = 32;
            var pointAy = 128;
            var pointBx = 1088;
            var pointBy = 832;
        } else if room == rm_mapForest {
            var pointAx = 256;
            var pointAy = 288;
            var pointBx = 1808;
            var pointBy = 1264;
        } else if room == rm_mapIce {
            var pointAx = 240;
            var pointAy = 304;
            var pointBx = 1312;
            var pointBy = 1296;
        }
        var distanceA = -1;
        var distanceB = -1;
        var foundIT = false;
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            var playersCount = instance_number(objDummieServer);
            for (var i = 0; i < playersCount; i++){
                var playerIT = instance_find(objDummieServer, i);
                if playerIT.isIT {
                    foundIT = true;
                    break;
                }
            }
            if foundIT {
                with(playerIT){
                    distanceA = distance_to_point(pointAx, pointAy);
                    distanceB = distance_to_point(pointBx, pointBy);
                }
                if distanceA > distanceB {
                    ServerSendGoodDice(pointAx, pointAy, socket);
                } else {
                    ServerSendGoodDice(pointBx, pointBy, socket);
                }
            }
        }
        break;
    case MSG_BARRIER:
        var hasBarrier = buffer_read(bufferServerReceived, buffer_bool);
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            inst.hasBarrier = hasBarrier;
            ServerSendBarrier(socket, inst.hasBarrier);
        }
        break;
    case MSG_BARRIER_REVERSE:
        var playersCount = instance_number(objDummieServer);
        var foundPlayer = false;
        var playerName = "";
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var playerUsedPower = ds_map_find_value(global.Clients, socket);
            playerName = playerUsedPower.playerName;
        }
        for (var i = 0; i < playersCount; i++){
            var playerIT = instance_find(objDummieServer, i);
            if playerIT.isIT  == true{
                foundPlayer = true;
                break;
            }
        }
        if foundPlayer {
            var text = string(playerName) + " used the barrier on " + string(playerIT.playerName) + "!";
            ServerSendChat(text, -1); // Tell everybody that this hero used the barrier on the IT player
            ServerSendBarrierReverse(playerIT.myID); // Tell the IT that some motherfucker put a barrier on him
            ServerSendBarrier(playerIT.myID, true); // Tell everybody to draw the barrier on the poor IT
        }
            break;
    // create the ice power
    case MSG_CREATE_FREEZA:
        var freezaDirection = buffer_read(bufferServerReceived, buffer_u8);
        var freezaX = buffer_read(bufferServerReceived, buffer_u32);
        var freezaY = buffer_read(bufferServerReceived, buffer_u32);
        var freeza = instance_create(freezaX, freezaY, objFreezaServer);
        freeza.directionFreeza = freezaDirection;
        break;
        
    // I should probably put this in a script, since it's kinda big.... but idk
    case MSG_BAD_DICE:
        var xPlayer = -1;
        var yPlayer = -1;
        var xTP = -1;
        var yTP = -1;
        
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var inst = ds_map_find_value(global.Clients, socket);
            xPlayer = inst.x;
            yPlayer = inst.y;
            var playersCount = instance_number(objDummieServer);
            var foundPlayer = false;
            for (var i = 0; i < playersCount; i++){
                var playerTP = instance_find(objDummieServer, i);
                if playerTP.isIT  == false and playerTP != inst{
                    foundPlayer = true;
                    xTP = playerTP.x;
                    yTP = playerTP.y;
                    break;
                }
            }
            if foundPlayer {
                ServerSendSound(soundEffects.badDice, xPlayer, yPlayer);
                ServerSendBadDice(xTP, yTP, socket);
                ServerSendBadDice(xPlayer, yPlayer, playerTP.myID);
                var text = string(inst.playerName) + " betrayed " + string(playerTP.playerName) + "!";
                ServerSendChat(text, -1); // Tell everybody that this motherfucker betrayed his friend
            }
        }
        break;    
        
    case MSG_STEAL_POWER:
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var evilPlayer = ds_map_find_value(global.Clients, socket);
            var playerNumber = instance_number(objDummieServer);
            var playerListSize = 0;
            var playerList;
            if(playerNumber >= 3) {
                for(var i = 0; i < playerNumber; i++){
                    var thisPlayer = instance_find(objDummieServer, i);
                    if(thisPlayer != evilPlayer and thisPlayer.isIT == false) {
                        playerList[playerListSize] = thisPlayer;
                        playerListSize++;
                    }
                }
                if(playerListSize > 0) {
                    var unluckyNumber = irandom(playerListSize - 1);
                    var thisPlayer = playerList[unluckyNumber];
                    var powerToSteal = thisPlayer.powerSelected;
                    ServerSendLosePower(thisPlayer.myID);
                    ServerSendStealPower(socket, powerToSteal);
                    var text = string(evilPlayer.playerName) + " stole a power from " + string(thisPlayer.playerName) + "!";
                    ServerSendChat(text, -1);
                }
            }
        }
        
        break;
        
    // Clones, clones everywhere
    case MSG_CLONE:
        var cloneID = buffer_read(bufferServerReceived, buffer_u32);
        var cloneX = buffer_read(bufferServerReceived, buffer_u16);
        var cloneY = buffer_read(bufferServerReceived, buffer_u16);
        var cloneImage = buffer_read(bufferServerReceived, buffer_u16);
        var cloneIsMoving = buffer_read(bufferServerReceived, buffer_bool);
        var cloneNumbers = instance_number(objDummieCloneServer);
        var cloneFound = false;
        if (!is_undefined(ds_map_find_value(global.Clients, socket))){
            var playerMaster = ds_map_find_value(global.Clients, socket); // the dude who created the clones
        }
        for (var i = 0; i < cloneNumbers; i++){
            var thisClone = instance_find(objDummieCloneServer, i);
            if (thisClone.myID == cloneID) {
                cloneFound = true;
                thisClone.x = cloneX;
                thisClone.y = cloneY;
                thisClone.image_index = cloneImage;
                thisClone.isMoving = cloneIsMoving;
                ServerSendClone(thisClone, socket);
                break;
            }
        }
        if (!cloneFound) {
            var thisClone = instance_create(cloneX, cloneY, objDummieCloneServer);
            with(thisClone){
                father = playerMaster; // The father objDummieServer id
                fatherID = socket; // The father's socket
                image_index = cloneImage;
                isMoving = cloneIsMoving;
                myID = cloneID;
                sprite_index = playerMaster.sprite_index;
                cloneHair = playerMaster.playerHair;
                cloneShirt = playerMaster.playerShirt;
                clonePants = playerMaster.playerPants;
                cloneShoes = playerMaster.playerShoes;
                cloneAcc = playerMaster.playerAcc;
                cloneHairColor = playerMaster.playerHairColor;
                cloneShirtColor = playerMaster.playerShirtColor;
                clonePantsColor = playerMaster.playerPantsColor;
                cloneShoesColor = playerMaster.playerShoesColor;
                cloneAccColor = playerMaster.playerAccColor;
                cloneName = playerMaster.playerName;
            }
            ServerSendClone(thisClone, socket);
        } 
        break;
    
    // Delete the clone
    case MSG_DELETE_CLONE:
        var cloneID = buffer_read(bufferServerReceived, buffer_u32);
        var cloneNumbers = instance_number(objDummieCloneServer);
        for (var i = 0; i < cloneNumbers; i++){
            var thisClone = instance_find(objDummieCloneServer, i);
            if (thisClone.myID == cloneID) {
                ServerSendDeleteClone(thisClone.myID, socket);
                with(thisClone){
                    instance_destroy();
                }
                break;
            }
        }
        break;
        
    // send the sound to everybody
    case MSG_SOUND:
        var soundNumber = buffer_read(bufferServerReceived, buffer_u8);
        var soundX = buffer_read(bufferServerReceived, buffer_u32);
        var soundY = buffer_read(bufferServerReceived, buffer_u32);
        ServerSendSound(soundNumber, soundX, soundY);
        break;
}
