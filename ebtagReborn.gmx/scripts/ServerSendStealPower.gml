/// ServerSendStealPower(playerSocket, powerToSteal)

var playerSocket = argument[0];
var powerToSteal = argument[1];

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_STEAL_POWER);
buffer_write(global.bufferServerWrite, buffer_s8, powerToSteal);

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
