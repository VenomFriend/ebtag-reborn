//Create the particle system for the ice spell
global.snowSystem = part_system_create();
global.snowType = part_type_create();
part_type_shape(global.snowType, pt_shape_snow);
part_type_life(global.snowType, room_speed * 0.5, room_speed);
part_type_size(global.snowType, 0.1, 0.15, 0.01, 0.01);
part_system_depth(global.snowSystem, -998);
