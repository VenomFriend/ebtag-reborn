/// ServerSendAllBlueWalls(socket)

var playerSocket = argument[0];

var blueWallsCount = instance_number(objBlueWallServer);

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_BLUE_WALLS);
buffer_write(global.bufferServerWrite, buffer_u16, blueWallsCount);

for(var i = 0; i < blueWallsCount; i++) {
    var blueWall = instance_find(objBlueWallServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, blueWall.id);
    buffer_write(global.bufferServerWrite, buffer_u32, blueWall.x);
    buffer_write(global.bufferServerWrite, buffer_u32, blueWall.y);
    buffer_write(global.bufferServerWrite, buffer_bool, blueWall.isSolid);
    buffer_write(global.bufferServerWrite, buffer_u8, blueWall.position);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

