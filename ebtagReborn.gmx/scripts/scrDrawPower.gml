if isVisible {
    switch(powerSelected){
        case playerPowers.theWall:
            sprite_index = sprFloorTheWall;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6);
            break;
        case playerPowers.goodDice:
            sprite_index = sprFloorGoodDice;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6); 
            break;
        case playerPowers.barrier:
            sprite_index = sprFloorBarrier;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6);
            break;
        case playerPowers.ghostbusters:
            sprite_index = sprFloorGhostbusters;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6); 
            break;
        case playerPowers.freeza:
            sprite_index = sprFloorFreeza;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6);
            break;
        case playerPowers.cloneWars:
            sprite_index = sprFloorCloneWars;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6);
            break;
        case playerPowers.compass:
            sprite_index = sprFloorCompass;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6); 
            break;
        case playerPowers.badDice:
            sprite_index = sprFloorBadDice;
            draw_sprite(sprPowerBorder, 0, x, y);
            draw_sprite(sprite_index, image_index, x + 6, y + 6);          
            break;
    }
}
