///ServerSendWall(wall)

var wall = argument[0];
var socketSize = ds_list_size(global.socketList);

// Tell everyone to create the wall
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_WALL);
buffer_write(global.bufferServerWrite, buffer_u16, 1);
buffer_write(global.bufferServerWrite, buffer_u32, wall.id);
buffer_write(global.bufferServerWrite, buffer_u32, wall.x);
buffer_write(global.bufferServerWrite, buffer_u32, wall.y);
buffer_write(global.bufferServerWrite, buffer_u32, wall.image_index);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
