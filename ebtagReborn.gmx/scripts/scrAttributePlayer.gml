image_speed = 0;
isIT = false;
myID = -1;
playerName = "";
isMoving = false; // is not moving
isInvisible = 0; // Since you become invisible/visible all the time, I need this.... 0 = visible, 1 = invisible and 2 = invisible reverse
isConfused = false; // If the player was affected by the confusion power
canMove = true; // Set to false for 3 seconds after become IT
sleepFrame = 0; // The current frame of the sleep animation.... yes, I know, this is an abomination
confusionFrame = 0; // The current frame of the confusion animation
hasBarrier = false; // Don't have a barrier
barrierFrame = -1; // The current frame of the barrier animation.... yes, I know, this is an abomination
powerSelected = -1; // Start with 0 powers


// The clothes
playerHair = -1;
playerHairColor = 0;
playerShoes = -1;
playerShoesColor = 0;
playerShirt = -1;
playerShirtColor = 0;
playerPants = -1;
playerPantsColor = 0;
playerAcc = -1;
playerAccColor = 0;
