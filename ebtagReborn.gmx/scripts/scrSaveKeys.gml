var keys = argument[0];

ini_open("char.sav");

ini_write_real("kb_controls", "up", keys.moveUpKey);
ini_write_real("kb_controls", "down", keys.moveDownKey);
ini_write_real("kb_controls", "left", keys.moveLeftKey);
ini_write_real("kb_controls", "right", keys.moveRightKey);
ini_write_real("kb_controls", "run", keys.runKey);
ini_write_real("kb_controls", "power", keys.usePowerKey);
ini_write_real("kb_controls", "powerBack", keys.changePowerBackKey);
ini_write_real("kb_controls", "powerForward", keys.changePowerForwardKey);
ini_write_real("kb_controls", "reject", keys.rejectPowerKey);

ini_close();
