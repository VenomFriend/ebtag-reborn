draw_self();
// Draw the hair
if (cloneHair) {
    draw_sprite_ext(cloneHair, image_index, x, y, image_xscale, image_yscale, image_angle, cloneHairColor, image_alpha);
}

// Draw the shirt
if (cloneShirt) {
    draw_sprite_ext(cloneShirt, image_index, x, y, image_xscale, image_yscale, image_angle, cloneShirtColor, image_alpha);
}

// Draw the pants
if (clonePants) {
    draw_sprite_ext(clonePants, image_index, x, y, image_xscale, image_yscale, image_angle, clonePantsColor, image_alpha);
}

// Draw the shoes
if (cloneShoes) {
    draw_sprite_ext(cloneShoes, image_index, x, y, image_xscale, image_yscale, image_angle, cloneShoesColor, image_alpha);
}

// Draw the accessories
if (cloneAcc) {
    draw_sprite_ext(cloneAcc, image_index, x, y, image_xscale, image_yscale, image_angle, cloneAccColor, image_alpha);
}
