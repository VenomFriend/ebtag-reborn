///ServerSendSound(soundNumber, x, y)
var soundNumber = argument[0];
var soundX = argument[1];
var soundY = argument[2];

var socketSize = ds_list_size(global.socketList);


buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SOUND);
buffer_write(global.bufferServerWrite, buffer_u8, soundNumber);
buffer_write(global.bufferServerWrite, buffer_u32, soundX);
buffer_write(global.bufferServerWrite, buffer_u32, soundY);


for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
