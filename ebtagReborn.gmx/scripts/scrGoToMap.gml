///scrGoToMap(mapToGo)

var mapToGo = argument[0];

switch(mapToGo) {
    case gameMap.forest:
        room_goto(rm_mapForest);
        break;
    case gameMap.ice:
        room_goto(rm_mapIce);
        break;
    case gameMap.dungeon:
        room_goto(rm_mapDungeon);
        break;
    case gameMap.desert:
        room_goto(rm_mapDesert);
        break;
}
