if isInvisible == 0 {
    draw_self();
    
    // Draw the hair
    if (playerHair) {
        draw_sprite_ext(playerHair, image_index, x, y, image_xscale, image_yscale, image_angle, playerHairColor, image_alpha);
    }
    
    // Draw the shirt
    if (playerShirt) {
        draw_sprite_ext(playerShirt, image_index, x, y, image_xscale, image_yscale, image_angle, playerShirtColor, image_alpha);
    }
    
    // Draw the pants
    if (playerPants) {
        draw_sprite_ext(playerPants, image_index, x, y, image_xscale, image_yscale, image_angle, playerPantsColor, image_alpha);
    }
    
    // Draw the shoes
    if (playerShoes) {
        draw_sprite_ext(playerShoes, image_index, x, y, image_xscale, image_yscale, image_angle, playerShoesColor, image_alpha);
    }
    
    // Draw the accessories
    if (playerAcc) {
        draw_sprite_ext(playerAcc, image_index, x, y, image_xscale, image_yscale, image_angle, playerAccColor, image_alpha);
    }

    if isIT {
        draw_sprite(sprIT, 0, x, y - 26);
    }
    if isConfused {
        draw_sprite(sprConfused, confusionFrame, x - 16, y - 38);
    }
    if !canMove {
        image_speed = 0;
        draw_sprite(sprSleeping, sleepFrame, x - 16, y - 38);
    }
    if barrierFrame != -1 {
        draw_sprite(sprBarrier, barrierFrame, x, y);
    }
    
    
} else if isInvisible == 1 and !isIT {
    if (isMoving) {
        draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.2); 
    } else {
        draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.05); 
    }
} else if(isInvisible == 2 and !isIT) {
    if (!isMoving) {
        draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.4); 
    } else {
        draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.02); 
    }
}
