/// ClientSendFreeza(directionPlayer, x, y)

var directionPlayer = argument[0];
var xToSend = argument[1];
var yToSend = argument[2];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_CREATE_FREEZA);
buffer_write(global.bufferClientWrite, buffer_u8, directionPlayer);
buffer_write(global.bufferClientWrite, buffer_u32, xToSend);
buffer_write(global.bufferClientWrite, buffer_u32, yToSend);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
