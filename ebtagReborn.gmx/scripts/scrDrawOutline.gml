///scrDrawOutline(x, y, size, text);

var xx = argument[0];
var yy = argument[1];
var size = argument[2];
var text = argument[3];


var color;
color = draw_get_color();
draw_set_color(c_black);
for(var i = xx - size; i <= xx + size; i += 1){
    for(var ii = yy - size; ii <= yy + size; ii+=1){
        draw_text(i, ii, text);
    }
}
draw_set_color(c_white);
draw_text(xx, yy, text);
draw_set_color(color);
