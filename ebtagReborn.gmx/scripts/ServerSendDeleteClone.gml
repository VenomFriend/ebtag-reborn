///ServerSendDeleteClone(cloneID, socket)
var cloneID = argument[0];
var playerSocket = argument[1];

var socketSize = ds_list_size(global.socketList);

// Tell everyone, except the clone master, to delete the clones

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_DELETE_CLONE);
buffer_write(global.bufferServerWrite, buffer_u32, cloneID);

// DO NOT SEND THE CLONE DELETE MESSAGE TO THE GUY WHO CREATED THEM
// AFTER ALL, HE IS THE ONE WHO SENDS THEIR INFO TO THE SERVER
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
