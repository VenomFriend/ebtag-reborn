var playerSocket = argument[0]; // socket = id
var hasBarrier = argument[1];
var socketSize = ds_list_size(global.socketList);

// Tell everybody that this player has a barrier
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_BARRIER);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);
buffer_write(global.bufferServerWrite, buffer_bool, hasBarrier);
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
