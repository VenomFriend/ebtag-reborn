///ClientSendSolarFlare(viewX, viewY)
// Send the Solar Flare 

var viewX = argument[0];
var viewY = argument[1];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_SOLAR_FLARE);
buffer_write(global.bufferClientWrite, buffer_u32, viewX);
buffer_write(global.bufferClientWrite, buffer_u32, viewY);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
