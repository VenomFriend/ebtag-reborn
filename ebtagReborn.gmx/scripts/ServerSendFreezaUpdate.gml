/// ServerSendFreezaUpdate(id, x, y)

var freezaID = argument[0];
var freezaX = argument[1];
var freezaY = argument[2];

var socketSize = ds_list_size(global.socketList);

// Tell everyone to update the freeza power
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_FREEZA);
buffer_write(global.bufferServerWrite, buffer_u32, freezaID);
buffer_write(global.bufferServerWrite, buffer_u32, freezaX);
buffer_write(global.bufferServerWrite, buffer_u32, freezaY);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
