/// ServerSendAllFriendWalls(socket)

var playerSocket = argument[0];

var friendWallsCount = instance_number(objFriendWallServer);
var friendButtonsCount = instance_number(objFriendButtonServer)

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_FRIEND_WALLS);
buffer_write(global.bufferServerWrite, buffer_u16, friendWallsCount);
buffer_write(global.bufferServerWrite, buffer_u16, friendButtonsCount);

for(var i = 0; i < friendWallsCount; i++) {
    var friendWall = instance_find(objFriendWallServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, friendWall.id);
    buffer_write(global.bufferServerWrite, buffer_u32, friendWall.x);
    buffer_write(global.bufferServerWrite, buffer_u32, friendWall.y);
    buffer_write(global.bufferServerWrite, buffer_bool, friendWall.isSolid);
    buffer_write(global.bufferServerWrite, buffer_u8, friendWall.position);
}

for(var i = 0; i < friendButtonsCount; i++) {
    var friendButton = instance_find(objFriendButtonServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, friendButton.id);
    buffer_write(global.bufferServerWrite, buffer_u32, friendButton.x);
    buffer_write(global.bufferServerWrite, buffer_u32, friendButton.y);
    buffer_write(global.bufferServerWrite, buffer_bool, friendButton.isPressed);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

