///ServerSendChangeMap(mapToGo, x, y);
var mapToGo = argument[0];
var xToGo = argument[1];
var yToGo = argument[2];
var socketSize = ds_list_size(global.socketList);

/// This here is gonna send the new map to everybody

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CHANGE_MAP);
buffer_write(global.bufferServerWrite, buffer_u8, mapToGo);
buffer_write(global.bufferServerWrite, buffer_u32, xToGo);
buffer_write(global.bufferServerWrite, buffer_u32, yToGo);


for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
