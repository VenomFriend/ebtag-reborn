///ServerSendVampirism(socket)

var playerSocket = argument[0]; // Remember: this socket = the player ID on the client side


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_VAMPIRISM);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) { // This message shouldn't be sent to the IT
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
