var character = argument[0];
var ip = argument[1];
var name = argument[2];

ini_open("char.sav");

ini_write_real("player", "Body", character.bodySelected);
ini_write_real("player", "Hair", character.hairSelected);
ini_write_real("player", "HairRed", character.hairRed);
ini_write_real("player", "HairGreen", character.hairGreen);
ini_write_real("player", "HairBlue", character.hairBlue);
ini_write_real("player", "Shirt", character.shirtSelected);
ini_write_real("player", "ShirtRed", character.shirtRed);
ini_write_real("player", "ShirtGreen", character.shirtGreen);
ini_write_real("player", "ShirtBlue", character.shirtBlue);
ini_write_real("player", "Pants", character.pantsSelected);
ini_write_real("player", "PantsRed", character.pantsRed);
ini_write_real("player", "PantsGreen", character.pantsGreen);
ini_write_real("player", "PantsBlue", character.pantsBlue);
ini_write_real("player", "Shoes", character.shoesSelected);
ini_write_real("player", "ShoesRed", character.shoesRed);
ini_write_real("player", "ShoesGreen", character.shoesGreen);
ini_write_real("player", "ShoesBlue", character.shoesBlue);
ini_write_real("player", "Acc", character.accSelected);
ini_write_real("player", "AccRed", character.accRed);
ini_write_real("player", "AccGreen", character.accGreen);
ini_write_real("player", "AccBlue", character.accBlue);
ini_write_real("player", "Sex", character.sex);
ini_write_string("info", "IP", ip.text);
ini_write_string("info", "Name", name.text);

ini_close();
