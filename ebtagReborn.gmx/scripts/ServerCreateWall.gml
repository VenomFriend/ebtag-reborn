///ServerCreateWall(direction,  wallX, wallY)

var playerFacing = argument[0];
var wallX = argument[1];
var wallY = argument[2];

switch(playerFacing) {
    case playerDirection.up:
    case playerDirection.down:
        var wall = instance_create(wallX, wallY, objWallServer);
        wall.image_index = 1;
        wall.alarm[1] = 1;
        break;
    case playerDirection.left:
    case playerDirection.right:
            var wall = instance_create(wallX, wallY, objWallServer);
            wall.image_index = 0;
            wall.alarm[1] = 1;
        break;
}
