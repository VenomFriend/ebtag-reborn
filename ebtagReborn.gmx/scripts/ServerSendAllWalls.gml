var playerSocket = argument[0];

var wallsCount = instance_number(objWallServer);

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_WALL);
buffer_write(global.bufferServerWrite, buffer_u16, wallsCount);

for(var i = 0; i < wallsCount; i++) {
    var wall = instance_find(objWallServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, wall.id);
    buffer_write(global.bufferServerWrite, buffer_u32, wall.x);
    buffer_write(global.bufferServerWrite, buffer_u32, wall.y);
    buffer_write(global.bufferServerWrite, buffer_u32, wall.image_index);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
