///ClientCreateWall(direction, player)

var playerFacing = argument[0];
var thisPlayer = argument[1];
var createdTheWall = false;
var wallX = -1;
var wallY = -1;

switch(playerFacing) {
    case playerDirection.up:
        var x0 = thisPlayer.x - 16;
        var x1 = thisPlayer.x + 15;
        var y0 = thisPlayer.y - 23;
        var y1 = thisPlayer.y - 8;

        
        if collision_rectangle(x0, y0, x1, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0, x1, y1, objWaterCollision, false, true) == noone{
            createdTheWall = true;
            wallX = x0;
            wallY = y0;
        } else {
            var moveALittle = 1;
            while(moveALittle < 33) {
                if collision_rectangle(x0 - moveALittle, y0, x1 - moveALittle, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0 - moveALittle, y0, x1 - moveALittle, y1, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0 - moveALittle;
                    wallY = y0;
                    break;
                }else if collision_rectangle(x0 + moveALittle, y0, x1 + moveALittle, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0 + moveALittle, y0, x1 + moveALittle, y1, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0 + moveALittle;
                    wallY = y0;
                    break;
                }
                moveALittle++;
            }
        }
        break;
    case playerDirection.down:
        var x0 = thisPlayer.x - 16;
        var x1 = thisPlayer.x + 15;
        var y0 = thisPlayer.y + 14
        var y1 = thisPlayer.y + 29;

        
        if collision_rectangle(x0, y0, x1, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0, x1, y1, objWaterCollision, false, true) == noone{
            createdTheWall = true;
            wallX = x0;
            wallY = y0;
        } else {
            var moveALittle = 1;
            while(moveALittle < 33) {
                if collision_rectangle(x0 - moveALittle, y0, x1 - moveALittle, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0 - moveALittle, y0, x1 - moveALittle, y1, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0 - moveALittle;
                    wallY = y0;
                    break;
                }else if collision_rectangle(x0 + moveALittle, y0, x1 + moveALittle, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0 + moveALittle, y0, x1 + moveALittle, y1, objWaterCollision, false, true) == noone{
                    createdTheWall= true;
                    wallX = x0 + moveALittle;
                    wallY = y0;
                    break;
                }
                moveALittle++;
            }
        }
        break;
    case playerDirection.left:
        var x0 = thisPlayer.x - 37;
        var x1 = thisPlayer.x - 20;
        var y0 = thisPlayer.y - 10
        var y1 = thisPlayer.y + 21;

        
        if collision_rectangle(x0, y0, x1, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0, x1, y1, objWaterCollision, false, true) == noone{
            createdTheWall = true;
            wallX = x0;
            wallY = y0;
        } else {
            var moveALittle = 1;
            while(moveALittle < 33) {
                if collision_rectangle(x0, y0 - moveALittle, x1, y1 - moveALittle, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0 - moveALittle, x1, y1 - moveALittle, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0;
                    wallY = y0 - moveALittle;
                    break;
                }else if collision_rectangle(x0, y0 + moveALittle, x1, y1 + moveALittle, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0 + moveALittle, x1, y1 + moveALittle, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0;
                    wallY = y0 + moveALittle;
                    break;
                }
                moveALittle++;
            }
        }
        break;
    case playerDirection.right:
        var x0 = thisPlayer.x + 12;
        var x1 = thisPlayer.x + 37;
        var y0 = thisPlayer.y - 10;
        var y1 = thisPlayer.y + 21;

        if collision_rectangle(x0, y0, x1, y1, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0, x1, y1, objWaterCollision, false, true) == noone{
            createdTheWall = true;
            wallX = x0;
            wallY = y0;
        } else {
            var moveALittle = 1;
            while(moveALittle < 33) {
                if collision_rectangle(x0, y0 - moveALittle, x1, y1 - moveALittle, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0 - moveALittle, x1, y1 - moveALittle, objWaterCollision, false, true) == noone{
                    createdTheWall= true;
                    wallX = x0;
                    wallY = y0 - moveALittle;
                    break;
                }else if collision_rectangle(x0, y0 + moveALittle, x1, y1 + moveALittle, objCollisionParent, false, true) == noone and collision_rectangle(x0, y0 + moveALittle, x1, y1 + moveALittle, objWaterCollision, false, true) == noone{
                    createdTheWall = true;
                    wallX = x0;
                    wallY = y0 + moveALittle;
                    break;
                }
                moveALittle++;
            }
        }
        break;
}

if(createdTheWall) {
    ClientSendSound(soundEffects.wall, thisPlayer.x, thisPlayer.y);
    ClientSendTheWall(directionPlayer, wallX, wallY);
    thisPlayer.special -= 30;
}
