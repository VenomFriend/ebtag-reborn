///ServerSendNewPlayer(ID, socket)
var thisPlayer = argument[0];
var playerSocket = argument[1];

var socketSize = ds_list_size(global.socketList);

/// This here is gonna send the coordinates/clothes/whatever 
/// of the player who just entered to everybody else on the server

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u16, 1);

//Write a really big packet with the new player information
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket);  //Remember, kids, your ID is your socket, not your player.id!
buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.x);
buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.y);
buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.sprite_index);
buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.image_index);
buffer_write(global.bufferServerWrite, buffer_s8, thisPlayer.playerHair);
buffer_write(global.bufferServerWrite, buffer_s8, thisPlayer.playerShirt);
buffer_write(global.bufferServerWrite, buffer_s8, thisPlayer.playerPants);
buffer_write(global.bufferServerWrite, buffer_s8, thisPlayer.playerShoes);
buffer_write(global.bufferServerWrite, buffer_s8, thisPlayer.playerAcc);
buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.playerHairColor);
buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.playerShirtColor);
buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.playerPantsColor);
buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.playerShoesColor);
buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.playerAccColor);
buffer_write(global.bufferServerWrite, buffer_string, thisPlayer.playerName);
buffer_write(global.bufferServerWrite, buffer_bool, thisPlayer.isIT);
buffer_write(global.bufferServerWrite, buffer_bool, thisPlayer.hasBarrier);
buffer_write(global.bufferServerWrite, buffer_u8, thisPlayer.isInvisible);
buffer_write(global.bufferServerWrite, buffer_bool, thisPlayer.isConfused);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
