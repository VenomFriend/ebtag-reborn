///ServerSendFriendWallUpdate(wall)
var thisFriendWall = argument[0];
var socketSize = ds_list_size(global.socketList);

// Tell everyone to update their friend walls
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_UPDATE_FRIEND_WALL);
buffer_write(global.bufferServerWrite, buffer_u32, thisFriendWall.id);
buffer_write(global.bufferServerWrite, buffer_bool, thisFriendWall.isSolid);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
