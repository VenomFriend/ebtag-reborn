///ServerSendClone(clone, socket)
var thisClone = argument[0];
var playerSocket = argument[1];

var socketSize = ds_list_size(global.socketList);

// This here is gonna send the clone to all the other players,
// except the one who created it 

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CLONE);

buffer_write(global.bufferServerWrite, buffer_u32, thisClone.myID);
buffer_write(global.bufferServerWrite, buffer_u16, thisClone.x);
buffer_write(global.bufferServerWrite, buffer_u16, thisClone.y);
buffer_write(global.bufferServerWrite, buffer_u16, thisClone.image_index);
buffer_write(global.bufferServerWrite, buffer_u32, thisClone.fatherID); // this is the socket of the player who created the clone

// DO NOT SEND THE CLONES TO THE GUY WHO CREATED THEM
// AFTER ALL, HE IS THE ONE WHO SENDS THEIR INFO TO THE SERVER
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
