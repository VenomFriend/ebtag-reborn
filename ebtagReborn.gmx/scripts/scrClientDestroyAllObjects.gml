///scrClientDestroyAllObjects();

with(objWallClient){
    instance_destroy();
}

with(objFreezaClient){
    instance_destroy();
}

with(objPowersClient){
    instance_destroy();
}

with(objBlueWallClient){
    instance_destroy();
}

with(objYellowWallClient){
    instance_destroy();
}

with(objFriendWallClient){
    instance_destroy();
}

with(objFriendButtonClient){
    instance_destroy();
}

with(objCloneClient){
    instance_destroy();
}

with(objDummieCloneClient){
    instance_destroy();
}

with(objTeleportClient){
    instance_destroy();
}

with(objTeleportCircleClient){
    instance_destroy();
}
