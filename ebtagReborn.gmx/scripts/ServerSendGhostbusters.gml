/// ServerSendGhostbusters(isInvisible, socket)
var isInvisible = argument[0];
var playerSocket = argument[1];


var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_GHOSTBUSTERS);
buffer_write(global.bufferServerWrite, buffer_u32, playerSocket); // Remember, kids: this socket is the  playerID on the Client Side!
buffer_write(global.bufferServerWrite, buffer_u8, isInvisible);


for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) { // I really don't need to send to the IT that he became invisible, he already knows that
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
