///scrCreatePlayer(player)
var player = argument[0];
var customize = instance_find(objCustomize, 0);


if (customize.sex == 0){ // male

    switch(customize.bodySelected) {
        case 1:
            player.sprite_index = sprBody1;
            break;
        case 2:
            player.sprite_index = sprBody2;
            break;
        case 3:
            player.sprite_index = sprBody3;
            break;
        case 4:
            player.sprite_index = sprBody4;
            break;
        case 5:
            player.sprite_index = sprBody5;
            break;
        case 6:
            player.sprite_index = sprBody6;
            break;
        case 7:
            player.sprite_index = sprBody7;
            break;
        case 8:
            player.sprite_index = sprBody8;
            break;
    }
    
    switch(customize.hairSelected) {
        case 1:
            player.playerHair = sprHair1;
            break;
        case 2:
            player.playerHair = sprHair2;
            break;
        case 3:
            player.playerHair = sprHair3;
            break;
        case 4:
            player.playerHair = sprHair4;
            break;
    }
    
    switch(customize.shirtSelected) {
        case 1:
            player.playerShirt = sprShirt1;
            break;
        case 2:
            player.playerShirt = sprShirt2;
            break;
    }
    
} else { // female
    switch(customize.bodySelected) {
        case 1:
            player.sprite_index = sprBodyF1;
            break;
        case 2:
            player.sprite_index = sprBodyF2;
            break;
        case 3:
            player.sprite_index = sprBodyF3;
            break;
        case 4:
            player.sprite_index = sprBodyF4;
            break;
        case 5:
            player.sprite_index = sprBodyF5;
            break;
        case 6:
            player.sprite_index = sprBodyF6;
            break;
        case 7:
            player.sprite_index = sprBodyF7;
            break;
        case 8:
            player.sprite_index = sprBodyF8;
            break;
    }
    
    switch(customize.hairSelected) {
        case 1:
            player.playerHair = sprHairF1;
            break;
        case 2:
            player.playerHair = sprHairF2;
            break;
        case 3:
            player.playerHair = sprHairF3;
            break;
        case 4:
            player.playerHair = sprHairF4;
            break;
        case 5:
            player.playerHair = sprHairF5;
            break;
    }
    
    switch(customize.shirtSelected) {
        case 1:
            player.playerShirt = sprShirtF1;
            break;
        case 2:
            player.playerShirt = sprShirtF2;
            break;
    }
}

switch(customize.pantsSelected) {
    case 1:
        player.playerPants = sprPants1;
        break;
}

switch(customize.shoesSelected) {
    case 1:
        player.playerShoes = sprShoes1;
        break;
}

switch(customize.accSelected) {
    case 1:
        player.playerAcc = sprAcc1;
        break;
    case 2:
        player.playerAcc = sprAcc2;
        break;
    case 3:
        player.playerAcc = sprAcc3;
        break;
}

player.playerHairColor = make_colour_rgb(customize.hairRed, customize.hairGreen, customize.hairBlue);
player.playerShirtColor = make_colour_rgb(customize.shirtRed, customize.shirtGreen, customize.shirtBlue);
player.playerPantsColor = make_colour_rgb(customize.pantsRed, customize.pantsGreen, customize.pantsBlue);
player.playerShoesColor = make_colour_rgb(customize.shoesRed, customize.shoesGreen, customize.shoesBlue);
player.playerAccColor = make_colour_rgb(customize.accRed, customize.accGreen, customize.accBlue);

player.playerName = global.playerName;

with(customize) {
    instance_destroy();
}
        
