///scrRadar()
draw_sprite(sprRadar, 0, view_xview[0] + view_wview[0] - 80, view_yview[0]);
var numberOfPlayers = instance_number(objDummieClient);
var numberOfClones = instance_number(objDummieCloneClient);

for (var i = 0; i < numberOfPlayers; i++) {
    var playerEnemy = instance_find(objDummieClient, i);
    with(myPlayerInstance) {
        if ( distance_to_object(playerEnemy) <= 504 and playerEnemy.isInvisible == 0) {
            var xRadar = round((playerEnemy.x - x) / 28);
            var yRadar = round((playerEnemy.y - y) / 28);
            if (xRadar + yRadar <= 24) {
                draw_sprite(sprRadar, 1,  view_xview[0] + view_wview[0] - 62 +  xRadar, view_yview[0] + 18 + yRadar);
            }
        }
    }
}

for (var i = 0; i < numberOfClones; i++) {
    var enemyClone = instance_find(objDummieCloneClient, i);
    with(myPlayerInstance) {
        if ( distance_to_object(enemyClone) <= 504) { //and !enemyClone.isInvisible
            var xRadar = round((enemyClone.x - x) / 28);
            var yRadar = round((enemyClone.y - y) / 28);
            if (xRadar + yRadar <= 24) {
                draw_sprite(sprRadar, 1,  view_xview[0] + view_wview[0] - 62 +  xRadar, view_yview[0] + 18 + yRadar);
            }
        }
    }
}
