///ClientSendGotPower(powerID)
var powerID = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_GOT_POWER);
buffer_write(global.bufferClientWrite, buffer_u32, powerID);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
