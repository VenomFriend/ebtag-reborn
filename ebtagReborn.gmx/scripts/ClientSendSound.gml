/// ClientSendSound(soundNumber, x, y)

var soundNumber = argument[0];
var soundX = argument[1];
var soundY = argument[2];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_SOUND);
buffer_write(global.bufferClientWrite, buffer_u8, soundNumber);
buffer_write(global.bufferClientWrite, buffer_u32, soundX);
buffer_write(global.bufferClientWrite, buffer_u32, soundY);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
