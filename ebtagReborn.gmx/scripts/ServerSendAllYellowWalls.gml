/// ServerSendAllYellowWalls(socket)

var playerSocket = argument[0];

var yellowWallsCount = instance_number(objYellowWallServer);

// Tell the people who just entered where the walls are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_YELLOW_WALLS);
buffer_write(global.bufferServerWrite, buffer_u16, yellowWallsCount);

for(var i = 0; i < yellowWallsCount; i++) {
    var yellowWall = instance_find(objYellowWallServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, yellowWall.id);
    buffer_write(global.bufferServerWrite, buffer_u32, yellowWall.x);
    buffer_write(global.bufferServerWrite, buffer_u32, yellowWall.y);
    buffer_write(global.bufferServerWrite, buffer_bool, yellowWall.isSolid);
    buffer_write(global.bufferServerWrite, buffer_u8, yellowWall.position);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

