/// ServerSendAllTeleports(socket)

var playerSocket = argument[0];

var teleportsCount = instance_number(objTeleportServer);

// Tell the people who just entered where the teleports are
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_TELEPORT);
buffer_write(global.bufferServerWrite, buffer_u16, teleportsCount);

for(var i = 0; i < teleportsCount; i++) {
    var teleport = instance_find(objTeleportServer, i);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.id);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.x);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.y);
    buffer_write(global.bufferServerWrite, buffer_u8, teleport.color);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToX);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToY);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToX2);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToY2);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToX3);
    buffer_write(global.bufferServerWrite, buffer_u32, teleport.goToY3);
    buffer_write(global.bufferServerWrite, buffer_u8, teleport.rotate);
}

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

