var key = argument[0];
var textKey = key;

switch(key) {
    case vk_up:
        textKey = "UP";
        break;
    case vk_down:
        textKey = "DOWN";
        break;
    case vk_left:
        textKey = "LEFT";
        break;
    case vk_right:
        textKey = "RIGHT";
        break;
    case vk_enter:
        textKey = "ENTER";
        break;
    case vk_space:
        textKey = "SPACE";
        break;
    case vk_shift:
        textKey = "SHIFT";
        break;
    case vk_control:
        textKey = "CTRL";
        break;
    case vk_alt:
        textKey = "ALT";
        break;
    case vk_backspace:
        textKey = "BCKSPACE";
        break;
    case vk_tab:
        textKey = "TAB";
        break;
    case vk_home:
        textKey = "HOME";
        break;
    case vk_end:
        textKey = "END";
        break;
    case vk_delete:
        textKey = "DEL";
        break;
    case vk_insert:
        textKey = "INSERT";
        break;
    case vk_pageup:
        textKey = "PAGE UP";
        break;
    case vk_pagedown:
        textKey = "PAGE DOWN";
        break;
    case vk_numpad0:
        textKey = "NUM 0";
        break;
    case vk_numpad1:
        textKey = "NUM 1";
        break;
    case vk_numpad2:
        textKey = "NUM 2";
        break;
    case vk_numpad3:
        textKey = "NUM 3";
        break;
    case vk_numpad4:
        textKey = "NUM 4";
        break;
    case vk_numpad5:
        textKey = "NUM 5";
        break;
    case vk_numpad6:
        textKey = "NUM 6";
        break;
    case vk_numpad7:
        textKey = "NUM 7";
        break;
    case vk_numpad8:
        textKey = "NUM 8";
        break;
    case vk_numpad9:
        textKey = "NUM 9";
        break;
    case vk_multiply:
        textKey = "NUM *";
        break;
    case vk_divide:
        textKey = "NUM /";
        break;
    case vk_add:
        textKey = "NUM +";
        break;
    case vk_subtract:
        textKey = "NUM -";
        break;
    case vk_decimal:
        textKey = "NUM ,";
        break;
    case ord('A'):
        textKey = "A";
        break;
    case ord('B'):
        textKey = "B";
        break;
    case ord('C'):
        textKey = "C";
        break;
    case ord('D'):
        textKey = "D";
        break;
    case ord('E'):
        textKey = "E";
        break;
    case ord('F'):
        textKey = "F";
        break;
    case ord('G'):
        textKey = "G";
        break;
    case ord('H'):
        textKey = "H";
        break;
    case ord('I'):
        textKey = "I";
        break;
    case ord('J'):
        textKey = "J";
        break;
    case ord('K'):
        textKey = "K";
        break;
    case ord('L'):
        textKey = "L";
        break;
    case ord('M'):
        textKey = "M";
        break;
    case ord('N'):
        textKey = "N";
        break;
    case ord('O'):
        textKey = "O";
        break;
    case ord('P'):
        textKey = "P";
        break;
    case ord('Q'):
        textKey = "Q";
        break;
    case ord('R'):
        textKey = "R";
        break;
    case ord('S'):
        textKey = "S";
        break;
    case ord('T'):
        textKey = "T";
        break;
    case ord('U'):
        textKey = "U";
        break;
    case ord('V'):
        textKey = "V";
        break;
    case ord('W'):
        textKey = "W";
        break;
    case ord('X'):
        textKey = "X";
        break;   
    case ord('Y'):
        textKey = "Y";
        break;   
    case ord('Z'):
        textKey = "Z";
        break;   
    case 188:
        textKey = ",";
        break; 
    case 190:
        textKey = ".";
        break; 
    case 191:
        textKey = ";";
        break; 
    case 193:
        textKey = "/";
        break; 
    case 222:
        textKey = "~";
        break;
    case 220:
        textKey = "]";
        break; 
    case 219:
        textKey = "´";
        break; 
    case 221:
        textKey = "[";
        break; 
    case 189:
        textKey = "-";
        break; 
    case 187:
        textKey = "=";
        break; 
}

return textKey;
