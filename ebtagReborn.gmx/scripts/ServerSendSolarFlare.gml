///ServerSendSolarFlare(viewX, viewY, ITGuy);
var viewX = argument[0];
var viewY = argument[1];
var playerSocket = argument[2];  // This is the IT guy ID (remember, the socket = his id)

var socketSize = ds_list_size(global.socketList);

// Tell everyone who is not IT to become blind
buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SOLAR_FLARE);
buffer_write(global.bufferServerWrite, buffer_u32, viewX);
buffer_write(global.bufferServerWrite, buffer_u32, viewY);

for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (thisSocket != playerSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
        }
}
