///scrServerDestroyAllObjects();

with(objWallServer){
    instance_destroy();
}

with(objFreezaServer){
    instance_destroy();
}

with(objPowersServer){
    instance_destroy();
}

with(objBlueWallServer){
    instance_destroy();
}

with(objYellowWallServer){
    instance_destroy();
}

with(objFriendWallServer){
    instance_destroy();
}

with(objFriendButtonServer){
    instance_destroy();
}

with(objDummieCloneServer){
    instance_destroy();
}

with(objTeleportServer){
    instance_destroy();
}

with(objTeleportCircleServer){
    instance_destroy();
}
