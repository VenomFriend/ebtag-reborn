/// ServerSendBadDice(xToTeleport, yToTeleport, playerSocket)

var xToTeleport = argument[0];
var yToTeleport = argument[1];
var playerSocket = argument[2];

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_BAD_DICE);
buffer_write(global.bufferServerWrite, buffer_u32, xToTeleport);
buffer_write(global.bufferServerWrite, buffer_u32, yToTeleport);

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
