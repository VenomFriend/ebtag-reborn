///ClientSendTheWall(playerFacing, wallX, wallY)
// Send The Wall

var playerFacing = argument[0];
//0 = up, 1 = down, 2 = left, 3 = right
var wallX = argument[1];
var wallY = argument[2];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_THE_WALL);
buffer_write(global.bufferClientWrite, buffer_u8, playerFacing);
buffer_write(global.bufferClientWrite, buffer_u32, wallX);
buffer_write(global.bufferClientWrite, buffer_u32, wallY);

network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
