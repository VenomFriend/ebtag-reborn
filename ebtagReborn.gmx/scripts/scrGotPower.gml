///scrGotPower(power)

var powerGot = argument[0];
var powerName = "";
switch(powerGot) {
    case(playerPowers.theWall):
        powerName = "The Wall";
        break;
    case(playerPowers.goodDice):
        powerName = "Good Dice";
        break;
    case(playerPowers.barrier):
        powerName = "Barrier";
        break;
    case(playerPowers.ghostbusters):
        powerName = "Ghostbusters";
        break;
    case(playerPowers.freeza):
        powerName = "Freeza";
        break;
    case(playerPowers.cloneWars):
        powerName = "Clone Wars";
        break;
    case(playerPowers.badDice):
        powerName = "Bad Dice";
        break;
}
var chatToSend = "You got the power " + powerName;
scrChatClient(chatToSend);
